const { app, BrowserWindow, Menu } = require("electron");

function createWindow() {

    //For modifying browser
    let window = new BrowserWindow({
      webPreferences: {
        nodeIntegration: true,
    }
    });
    
    Menu.setApplicationMenu(null);

    //loading User Interface file
    window.loadFile("app/login.html");

    //For using devtools
    //window.webContents.openDevTools();

    window.on('closed', () => {window = null})

    
}

// Initialization and is ready to create browser windows.
app.whenReady().then(createWindow);


// Below code is for MacOS

app.on("window-all-closed", () => {
    if (process.platform !== "darwin") {
      app.quit();
    }
});

app.on("activate", () => {
    if (window === null) {
      createWindow();
    }
});

