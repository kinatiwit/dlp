// Requirement modules
const joins = require('path').join
const dirname = require('path').dirname
const chokidar = require('chokidar')
const fs = require('fs')
const csv = require('csv-parse')
const _ = require('lodash')
const CryptoJS = require("crypto-js")
const nodemailer = require('nodemailer')
const shell = require('node-powershell')

//For using jquery
window.$ = window.jQuery = require("jquery")

// Checking changes in the pii inventory
$(document).ready(async () => 
    
    {
        if(!_.isEqual(sensitive_data,prev_sensitive_data)){
            pii_compare()
            pii_data.prev_sensitive_data = sensitive_data
            var updated_data =  JSON.stringify(pii_data)
            fs.writeFileSync(inventory_path,updated_data)
        } 
    }

    

    
)


//pii_inventory path
const inventory_path = joins(__dirname,'../app/pii_inventory/pii_data.json')

// Use to watch the change in pii_inventory
const inventory_watcher = chokidar.watch(inventory_path)

//use to get user data
const db_path = joins(__dirname,'../app/db/users.json')
var users_data = JSON.parse(fs.readFileSync(db_path))

// Event handler for inventory watcher
inventory_watcher
    .on("ready", async () =>{     
        localStorage.setItem('not_delete', false)
        watchlist_add()
    })

//Read data from pii_inventory
var pii_data = JSON.parse(fs.readFileSync(inventory_path))
var sensitive_data = pii_data.sensitive_data
var prev_sensitive_data = pii_data.prev_sensitive_data

var watch_list = []

async function watchlist_add(){
    watch_pii()
    for( path of watch_list){
        await lock_file(path)
    }
    console.log(watch_list)
  }
  
// Function that use to show the file in the watch list and put the file to watcher

function watch_pii(){
    
    $("#watch_files tbody").empty()

    for( i = 0; i < Object.keys(sensitive_data).length; i++){

        curr_data = Object.values(sensitive_data)[i]
        path = curr_data.file_path.replaceAll("/","\\")

        watch_list.push(path)

        var folder_path =  dirname(curr_data.file_path)
        folder_watcher.add(folder_path)
        watch_table_append(curr_data.file_name)   
             
    }
  
}

function watch_table_append(file_name){

    var pii_data = (_.find(sensitive_data, {"file_name":file_name}))
    
    $("#watch_files tbody").append(
        '<tr>'+
            '<td>'+pii_data.file_name+'</td>'+
            '<td>'+pii_data.file_path+'</td>'+
            '<td>'+pii_data.data_type+'</td>'+
            '<td>'+pii_data.data_owner+'</td>'+
            '<td>'+pii_data.level+'</td>'+
            '<td>'+pii_data.scan_date+'</td>'+
        '</tr>'
    )
        
}

const folder_watcher = chokidar.watch()

folder_watcher
    .on('ready',() => console.log('Folder watcher is ready'))
    .on('change',async path => {
        path
        if(_.includes(watch_list,path)){
            localStorage.setItem('not_delete', true)
            lock_file(path.replaceAll("/","\\"))
            
            update_log(
                path + ', ' +
                'File Being Edited' + ', ' +
                path +' has been edited' + ', ' +
                today
            )
            
        }
        console.log(watch_list)
    })
    .on('unlink', async path => {
        if(_.includes(watch_list,path) && !localStorage.getItem('not_delete')){
            update_log(
                path + ', ' +
                'File Being Deleted' + ', ' +
                path +' has been deleted' + ', ' +
                today
            )

        }
    })
    .on('add', () => {
        var watchedPaths = folder_watcher.getWatched();
        console.log(watchedPaths)
    })


//Encrytion Section

const key = 'key'

function file_encrypt(file){
    var originaltext = String(fs.readFileSync(file))
    var ciphertext = CryptoJS.AES.encrypt(originaltext,key).toString()
    fs.writeFile(file, ciphertext, err => {
        if (err) {
            return console.log(err);
          }
    })
}

function file_decrypt(file){
    var ciphertext = String(fs.readFileSync(file))
    var originaltext  = CryptoJS.AES.decrypt(ciphertext, key).toString(CryptoJS.enc.Utf8);
    fs.writeFile(file, originaltext, err => {
        if (err) {
            return console.log(err);
          }
    })
}



//file_encrypt()
//file_decrypt()


function lock_file(path){

    let ps = new shell({
        executionPolicy: 'Bypass',
        noProfile: true
        });
        
        ps.addCommand('$acl = get-acl '+path)
        ps.addCommand('$AccessRule = New-Object system.security.AccessControl.FileSystemAccessRule("Everyone","FullControl","Deny")')
        ps.addCommand('$acl.AddAccessRule($AccessRule)')
        ps.addCommand('set-acl -AclObject $acl '+ path)
        
        ps.invoke().then(output => {
        console.log(output);
        }).catch(err => {
        console.log(err);
        ps.dispose();
        
    })
    
}


//Add file to the watch lsit


async function add_file(){

    var file_path = document.getElementById("file").files[0].path
    var file_type = document.getElementById("file").files[0].type
    var file_name = file_path.replace(/^.*[\\\/]/, '')
    var today = new Date()

    console.log(file_path)

    var new_file = {
        "id" : parseInt(_.last(sensitive_data).id) + 1,
        "file_name": file_name,
        "file_path": file_path.replace(/\\/g, "/"),
        "data_type": file_type,
        "data_owner": $("#data_owner").val(),
        "level":$("#sensitive_level").val(), 
        "scan_date": today,
        "need_encrypted": false
    }
    
    
    if($("#sensitive_level").val() == "HIGHLY SENSITIVE"){
        file_encrypt(file_path)
    }
    
    pii_data.sensitive_data.push(new_file)
    pii_data.prev_sensitive_data.push(new_file)
    var updated_data = JSON.stringify(pii_data)
    fs.writeFileSync(inventory_path,updated_data )
    
    
    update_log(
        file_path + ', ' +
        'File Has Been Added to Watch List' + ', ' +
        path +' has been added to watch' + ', ' +
        today
    )
    

    console.log(file_path)

}

function sign_out(){
    window.location.replace('login.html')
}
//console.log(pii_data)

users_data.users.forEach(element =>{
    $("#data_owner").append(
        '<option>'+element.name+'</option>'
    )
})


async function change_email(){

    users_data.admin_email = $('#new_mail').val()
    var updated_data = JSON.stringify(users_data)
    fs.writeFileSync(db_path,updated_data)
    window.location.reload()
    
    
}