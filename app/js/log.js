//Read a log
var log_path = joins(__dirname, "../app/pii_inventory/log/log.csv");
var log_data = [];

fs.createReadStream(log_path)
  .pipe(csv({ columns: true }))
  .on("data", (data) => {
    log_data.push(data);
    show_log(data);
    //console.log(data.File_Name)
  })
  .on("end", () => console.log("Log reading finished"));

//Show log on the table
function show_log(data) {
  //$("#log_table tbody").empty()
  $("#log_table tbody").append(
    "<tr>" +
      "<td>" +
      data.File_Name +
      "</td>" +
      "<td>" +
      data.Event +
      "</td>" +
      "<td>" +
      data.Description +
      "</td>" +
      "<td>" +
      data.Date +
      "</td>" +
      "</tr>"
  );
}

// Function for updating the log
let only_curr = _.differenceWith(
  sensitive_data,
  prev_sensitive_data,
  _.isEqual
);
let only_prev = _.differenceWith(
  prev_sensitive_data,
  sensitive_data,
  _.isEqual
);

var today = new Date().toString();

function pii_compare() {
  for (index = 0; index < Object.keys(only_curr).length; index++) {
    var curr_data = Object.values(only_curr)[index];
    //console.log(curr_data)
    if (prev_id_exist(curr_data.id)) {
      if (prev_data.data_type != curr_data.data_type) {
        update_log(
          curr_data.file_name +
            ", " +
            "pii inventory edited" +
            ", " +
            curr_data.file_name +
            " data type has been changed  from " +
            prev_data.data_type +
            " to " +
            curr_data.data_type +
            ", " +
            today
        );
      }

      if (prev_data.file_path != curr_data.file_path) {
        update_log(
          curr_data.file_name +
            ", " +
            "pii inventory edited" +
            ", " +
            curr_data.file_name +
            " file path has been changed  from " +
            prev_data.file_path +
            " to " +
            curr_data.file_path +
            ", " +
            today
        );
      }

      if (prev_data.data_owner != curr_data.data_owner) {
        update_log(
          curr_data.file_name +
            ", " +
            "pii inventory edited" +
            ", " +
            curr_data.file_name +
            " data owner has been changed  from " +
            prev_data.data_owner +
            " to " +
            curr_data.data_owner +
            ", " +
            today
        );
      }

      if (prev_data.scan_date != curr_data.scan_date) {
        update_log(
          curr_data.file_name +
            ", " +
            "pii inventory edited" +
            ", " +
            curr_data.file_name +
            " scan_date has been changed  from " +
            prev_data.scan_date +
            " to " +
            curr_data.scan_date +
            ", " +
            today
        );
      }
      if (prev_data.level != curr_data.level) {
        update_log(
          curr_data.file_name +
            ", " +
            "pii inventory edited" +
            ", " +
            curr_data.file_name +
            " sensitivity llevel has been changed  from " +
            prev_data.level +
            " to " +
            curr_data.level +
            ", " +
            today
        );
      }
    } else {
      update_log(
        curr_data.file_name +
          ", " +
          "file has been added to pii inventory" +
          ", " +
          curr_data.file_name +
          " has beend added" +
          ", " +
          today
      );
    }
  }
}

function prev_id_exist(curr_id) {
  for (index = 0; index < Object.keys(only_prev).length; index++) {
    prev_data = Object.values(only_prev)[index];
    if (prev_data.id === curr_id) {
      return true;
    }
  }
}

//Write a log & send email

function update_log(new_log) {
  send_mail(new_log.split(",")[1], new_log.split(",")[2]);

  fs.appendFile(log_path, "\n" + new_log, (err) => {
    if (err) console.error("Couldn't append the data");
    console.log("The data was appended to file!");
  });
}

async function send_mail(subject, text) {
  UIkit.modal(loading_box).show();
  let transporter = nodemailer.createTransport({
    service: "Gmail",
    auth: {
      user: "",
      pass: "",
    },
  });

  let info = await transporter.sendMail({
    from: "", // sender address
    to: users_data.admin_email, // list of receivers
    subject: subject, // Subject line
    text: text,
  });

  console.log("Message sent: %s", info.messageId);
  window.location.reload();
}
