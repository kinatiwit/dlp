const requests_path = joins(__dirname,'../app/db/requests.json')

var requests_data = JSON.parse(fs.readFileSync(requests_path))
var requests = requests_data.requests

requests.forEach(element => {  

    if(element.status ==="pending"){
        $("#request_table tbody").append(
            '<tr>'+
                '<td>'+element.file_path+'</td>'+
                '<td>'+element.request_type+'</td>'+
                '<td>'+element.requester+'</td>'+
                '<td>'+element.status+'</td>'+
                '<td>'+
                    '<img onClick="update_request(' + element.id+ ',`Approved\`)" class ="uk-padding uk-padding-remove-vertical" src="img/tick.svg" >'+
                    '<img onClick="update_request(' + element.id + ',`Denied`)" class ="uk-padding uk-padding-remove-vertical" src="img/cross.svg" >'+
                '</td>'+
            '</tr>'
        )
    }else{
        $("#request_table tbody").append(
            '<tr>'+
                '<td>'+element.file_path+'</td>'+
                '<td>'+element.request_type+'</td>'+
                '<td>'+element.requester+'</td>'+
                '<td>'+element.status+'</td>'+
            '</tr>'
        )
    }
     
})

function update_request(id,status){

    var request = _.find(requests,{"id":id})
    var index = _.findIndex(requests,{"id":id})

    request.status = status
    
    requests_data.requests.splice(index,1,request)

    var updated_data = JSON.stringify(requests_data)
    fs.writeFileSync(requests_path,updated_data)
    
    if(status == "Approved"){
        update_log(
            request.file_path + ', ' +
            'Request Approved' + ', ' +
            request.file_path+' has been approved to be accessed' + ', ' +
            today
        )
    }else if(status == "Denied"){
        update_log(
            request.file_path + ', ' +
            'Request Denied' + ', ' +
            request.file_path+' has been denied to be accessed' + ', ' +
            today
        )
    }
    
}
