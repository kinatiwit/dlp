$( "#request_bar" ).click(function() {

    $( this ).removeClass("inactive_tab")
    $( this ).addClass("active_tab")
    $("#request_bar img").attr('src','img/active_request.svg')
    $("#request_bar h3").css("color", "white")
    $("#request").css("display", "block")
    
    $("#access_bar").removeClass("active_tab")
    $("#access_bar").addClass("inactive_tab")
    $("#access_bar img").attr('src','img/inactive_status.svg')
    $("#access_bar h3").css("color", "#383c4a")
    $("#status").css("display", "none")
    
})

$( "#access_bar" ).click(function() {
    $("#request_bar").removeClass("active_tab")
    $("#request_bar").addClass("inactive_tab")
    $("#request_bar img").attr('src','img/inactive_request.svg')
    $("#request_bar h3").css("color", "#383c4a")
    $("#request").css("display", "none")

    $( this ).removeClass("inactive_tab")
    $( this ).addClass("active_tab")
    $("#access_bar img").attr('src','img/active_status.svg')
    $("#access_bar h3").css("color", "white")
    $("#status").css("display", "block")

});

console.log(localStorage.getItem('name')) 