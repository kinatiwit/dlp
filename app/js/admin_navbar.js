$( "#list_bar" ).click(function() {

    $( this ).removeClass("inactive_tab")
    $( this ).addClass("active_tab")
    $("#list_bar img").attr('src','img/active_watch.svg')
    $("#list_bar h3").css("color", "white")
    $("#watching_files").css("display", "block")
    
    $("#log_bar").removeClass("active_tab")
    $("#log_bar").addClass("inactive_tab")
    $("#log_bar img").attr('src','img/inactive_log.svg')
    $("#log_bar h3").css("color", "#383c4a")
    $("#transaction_log").css("display", "none")
    
    $("#access_bar").removeClass("active_tab")
    $("#access_bar").addClass("inactive_tab")
    $("#access_bar img").attr('src','img/inactive_request.svg')
    $("#access_bar h3").css("color", "#383c4a")
    $("#access").css("display", "none")
    
})

$( "#log_bar" ).click(function() {
    $("#list_bar").removeClass("active_tab")
    $("#list_bar").addClass("inactive_tab")
    $("#list_bar img").attr('src','img/inactive_watch.svg')
    $("#list_bar h3").css("color", "#383c4a")
    $("#watching_files").css("display", "none")

    $( this ).removeClass("inactive_tab")
    $( this ).addClass("active_tab")
    $("#log_bar img").attr('src','img/active_log.svg')
    $("#log_bar h3").css("color", "white")
    $("#transaction_log").css("display", "block")

    $("#access_bar").removeClass("active_tab")
    $("#access_bar").addClass("inactive_tab")
    $("#access_bar img").attr('src','img/inactive_request.svg')
    $("#access_bar h3").css("color", "#383c4a")
    $("#access").css("display", "none")
});

$( "#access_bar" ).click(function() {
    $("#list_bar").removeClass("active_tab")
    $("#list_bar").addClass("inactive_tab")
    $("#list_bar img").attr('src','img/inactive_watch.svg')
    $("#list_bar h3").css("color", "#383c4a")
    $("#watching_files").css("display", "none")

    $("#log_bar").removeClass("active_tab")
    $("#log_bar").addClass("inactive_tab")
    $("#log_bar img").attr('src','img/inactive_log.svg')
    $("#log_bar h3").css("color", "#383c4a")
    $("#transaction_log").css("display", "none")

    $("#access_bar").removeClass("inactive_tab")
    $("#access_bar").addClass("active_tab")
    $("#access_bar img").attr('src','img/active_request.svg')
    $("#access_bar h3").css("color", "white")
    $("#access").css("display", "block")
});