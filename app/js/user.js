const joins = require('path').join
const chokidar = require('chokidar')
const fs = require('fs')
const csv = require('csv-parse')
const _ = require('lodash')
const CryptoJS = require("crypto-js")
const nodemailer = require('nodemailer')
const shell = require('node-powershell')

window.$ = window.jQuery = require("jquery")

const inventory_path = joins(__dirname, '../app/pii_inventory/pii_data.json')
const requests_path = joins(__dirname, '../app/db/requests.json')

var pii_data = JSON.parse(fs.readFileSync(inventory_path))
var sensitive_data = pii_data.sensitive_data
var prev_sensitive_data = pii_data.prev_sensitive_data

var requests_data = JSON.parse(fs.readFileSync(requests_path))
var requests = requests_data.requests

const db_path = joins(__dirname,'../app/db/users.json')
var users_data = JSON.parse(fs.readFileSync(db_path))

console.log(requests_data.requests)

const name = localStorage.getItem('name')

console.log(sensitive_data)

sensitive_data.forEach(element => {
    console.log(element)
    if (element.data_owner === name) {

        $("#files_list tbody").append(
            '<tr>' +
            '<td>' + element.file_name + '</td>' +
            '<td>' + element.file_path + '</td>' +
            '<td>' +
            '<button class="uk-button uk-button-default" type="button">Request</button>' +
            '<div uk-dropdown="animation: uk-animation-slide-top-small; duration: 500">' +
            '<ul class="uk-nav uk-dropdown-nav">' +
            '<li onClick="read_request(\'' + element.file_path + '\')" ><a>Read</a></li>' +
            '<li onClick="edit_request(\'' + element.file_path + '\')" ><a>Edit</a></li>' +
            '<li onClick="delete_request(\'' + element.file_path + '\')" ><a>Delete</a></li>' +
            '</ul>' +
            '</td>' +
            '</tr>'
        )

    }
})

requests.forEach(element => {
    if (element.requester === name) {
        if (element.status === "Approved") {
            if (element.request_type == "delete") {
                $("#status_list tbody").append(
                    '<tr>' +
                    '<td>' + element.file_path + '</td>' +
                    '<td>' + element.request_type + '</td>' +
                    '<td>' + element.status + '</td>' +
                    '<td><button  onClick="delete_file(' + element.id + ',\'' + element.file_path + '\')" class="uk-button uk-button-default">Delete</button></td>' +
                    '</tr>'
                )
            } else {
                $("#status_list tbody").append(
                    '<tr>' +
                    '<td>' + element.file_path + '</td>' +
                    '<td>' + element.request_type + '</td>' +
                    '<td>' + element.status + '</td>' +
                    '<td><button  onClick="unlock_file(' + element.id + ',\'' + element.file_path + '\')" class="uk-button uk-button-default">Unlock</button></td>' +
                    '</tr>'
                )
            }

        } else if (element.status === "Unlocked") {
            $("#status_list tbody").append(
                '<tr>' +
                '<td>' + element.file_path + '</td>' +
                '<td>' + element.request_type + '</td>' +
                '<td>' + element.status + '</td>' +
                '<td><button  onClick="lock_file(' + element.id + ',\'' + element.file_path + '\')" class="uk-button uk-button-default">Lock</button></td>' +
                '</tr>'
            )
        }
        else {
            $("#status_list tbody").append(
                '<tr>' +
                '<td>' + element.file_path + '</td>' +
                '<td>' + element.request_type + '</td>' +
                '<td>' + element.status + '</td>' +
                '</tr>'
            )
        }
    }
})

function read_request(path) {

    var new_requests = {
        "id": parseInt(_.last(requests).id) + 1,
        "file_path": path,
        "requester": name,
        "request_type": "read",
        "status": "pending"
    }

    requests_data.requests.push(new_requests)
    var updated_data = JSON.stringify(requests_data)
    fs.writeFileSync(requests_path, updated_data)

    update_log(
        path + ', ' +
        path +' has been request to be read' + ', ' +
        new_requests.requester + ' has request for reading ' + path +', ' +
        today
    )
}

function edit_request(path) {

    var new_requests = {
        "id": parseInt(_.last(requests).id) + 1,
        "file_path": path,
        "requester": name,
        "request_type": "edit",
        "status": "pending"
    }

    requests_data.requests.push(new_requests)
    var updated_data = JSON.stringify(requests_data)
    fs.writeFileSync(requests_path, updated_data)

    update_log(
        path + ', ' +
        path +' has been request to be editted' + ', ' +
        new_requests.requester + ' has request for editing ' + path +', ' +
        today
    )
}

function delete_request(path) {

    var new_requests = {
        "id": parseInt(_.last(requests).id) + 1,
        "file_path": path,
        "requester": name,
        "request_type": "delete",
        "status": "pending"
    }

    requests_data.requests.push(new_requests)
    var updated_data = JSON.stringify(requests_data)
    fs.writeFileSync(requests_path, updated_data)

    update_log(
        path + ', ' +
        path +' has been request to be deleted' + ', ' +
        new_requests.requester + ' has request for deleting ' + path +', ' +
        today
    )
}


async function unlock_file(id, path) {

    update_request(id, "Unlocked")

    var file_path = path.replaceAll("/", "\\")
    var file_data = _.find(sensitive_data, { "file_path": path })

    var request = _.find(requests, { "id": id })

    let ps = new shell({
        executionPolicy: 'Bypass',
        noProfile: true
    })

    ps.addCommand('$acl = get-acl ' + file_path)
    ps.addCommand('$OldRule = New-Object system.security.AccessControl.FileSystemAccessRule("Everyone","FullControl","Deny")')
    ps.addCommand('$acl.RemoveAccessRule($OldRule)')
    ps.addCommand('$AccessRule = New-Object system.security.AccessControl.FileSystemAccessRule("Everyone","FullControl","Allow")')
    ps.addCommand('$acl.AddAccessRule($AccessRule)')
    ps.addCommand('set-acl -AclObject $acl ' + file_path)

    ps.invoke()
        .then(output => {   
            if (file_data.level == "HIGHLY SENSITIVE") {
                file_decrypt(path)
            }
            if(request.request_type == "read"){
                fs.chmodSync(path,0o400)
            }
            update_log(
                path + ', ' +
                path +' has been unlocked' + ', ' +
                file_data.data_owner + ' unlock ' + path +', ' +
                today
            )
            console.log(output)
        })
        .catch(err => {
            console.log(err);
            ps.dispose();

        })

}

async function lock_file(id, path) {

    update_request(id, "Locked")

    var file_path = path.replaceAll("/", "\\")
    var file_data = _.find(sensitive_data, { "file_path": path })

    var request = _.find(requests, { "id": id })

    if(request.request_type == "read"){
        fs.chmodSync(path,0o777)
    }

    file_encrypt(path)

    let ps = new shell({
        executionPolicy: 'Bypass',
        noProfile: true
    })

    ps.addCommand('$acl = get-acl ' + file_path)
    ps.addCommand('$OldRule = New-Object system.security.AccessControl.FileSystemAccessRule("Everyone","FullControl","Allow")')
    ps.addCommand('$acl.RemoveAccessRule($OldRule)')
    ps.addCommand('$AccessRule = New-Object system.security.AccessControl.FileSystemAccessRule("Everyone","FullControl","Deny")')
    ps.addCommand('$acl.AddAccessRule($AccessRule)')
    ps.addCommand('set-acl -AclObject $acl ' + file_path)

    ps.invoke().then(output => {
        update_log(
            path + ', ' +
            path +' has been locked' + ', ' +
            file_data.data_owner + ' lock ' + path +', ' +
            today
        )
    }).catch(err => {
        console.log(err);
        ps.dispose();

    });

}

function update_request(id, status) {

    var request = _.find(requests, { "id": id })
    var index = _.findIndex(requests, { "id": id })

    request.status = status

    requests_data.requests.splice(index, 1, request)

    var updated_data = JSON.stringify(requests_data)
    fs.writeFileSync(requests_path, updated_data)

}


async function delete_file(id, path) {

    update_request(id, "Deleted")

    var file_path = path.replaceAll("/", "\\")
    var this_file = _.find(sensitive_data, { "file_path": path })
    var remaining_files = _.reject(sensitive_data, { "file_path": path })

    pii_data.sensitive_data = remaining_files
    pii_data.prev_sensitive_data = remaining_files

    var updated_data = JSON.stringify(pii_data)
    fs.writeFileSync(inventory_path, updated_data)

    let ps = new shell({
        executionPolicy: 'Bypass',
        noProfile: true
    })

    ps.addCommand('$acl = get-acl ' + file_path)
    ps.addCommand('$OldRule = New-Object system.security.AccessControl.FileSystemAccessRule("Everyone","FullControl","Deny")')
    ps.addCommand('$acl.RemoveAccessRule($OldRule)')
    ps.addCommand('$AccessRule = New-Object system.security.AccessControl.FileSystemAccessRule("Everyone","FullControl","Allow")')
    ps.addCommand('$acl.AddAccessRule($AccessRule)')
    ps.addCommand('set-acl -AclObject $acl ' + file_path)
    ps.addCommand('Remove-Item ' + file_path)

    ps.invoke().then(output => {
        console.log(output);
        update_log(
            path + ', ' +
            path +' has been locked' + ', ' +
            this_file.data_owner + ' has deleted ' + path +', ' +
            today
        )
    }).catch(err => {
        console.log(err);
        ps.dispose();

    });
}

const key = 'key'

function file_encrypt(file) {
    var originaltext = String(fs.readFileSync(file))
    var ciphertext = CryptoJS.AES.encrypt(originaltext, key).toString()
    fs.writeFile(file, ciphertext, err => {
        if (err) {
            return console.log(err);
        }
    })
}

function file_decrypt(file) {
    var ciphertext = String(fs.readFileSync(file))
    var originaltext = CryptoJS.AES.decrypt(ciphertext, key).toString(CryptoJS.enc.Utf8);
    fs.writeFile(file, originaltext, err => {
        if (err) {
            return console.log(err);
        }
    })
}

function sign_out(){
    window.location.replace('login.html')
}


