const fs = require('fs')
const joins = require('path').join
const _ = require('lodash')

window.$ = window.jQuery = require("jquery")

localStorage.clear()

const db_path = joins(__dirname,'../app/db/users.json')

var users_data = JSON.parse(fs.readFileSync(db_path))

$( "#login" ).click(function() {
    var username = $("#username").val()
    var password = $("#password").val() 

    if(username == "admin" && password == "password"){
        window.location.replace('admin.html')
    }
    else if(_.find(users_data.users, {"id":username,"pass":password}) ){
        user = _.filter(users_data.users, _.matches({"id":username,"pass":password}))
        localStorage.setItem('name', user[0].name)
        localStorage.setItem('id', user[0].id)
        window.location.replace('user.html')
    }
    else{
        alert("wrong username or password")
    }
    
  });


function add_user(){
    var new_user = {
        "id" : $("#new_id").val(),
        "pass": $("#new_pass").val(),
        "name": $("#name").val(),
    }

    users_data.users.push(new_user)
    var updated_data = JSON.stringify(users_data)
    fs.writeFileSync(db_path,updated_data )
    window.location.reload()
}